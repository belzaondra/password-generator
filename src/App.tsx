import React, { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { RiLockPasswordFill } from "react-icons/ri";
import { toast } from "react-toastify";
import "./App.css";
import { Switch } from "./Components/Switch";
import { generatePassword as passwordGenerator } from "./Utils/passwordGenerator";

export interface Settings {
  uppercase: boolean;
  lowercase: boolean;
  numbers: boolean;
  symbols: boolean;
}

function App() {
  const [password, setPassword] = useState<string | null>(null);
  const [length, setLength] = useState<number>(16);
  const [settings, setSettings] = useState<Settings>({
    uppercase: true,
    lowercase: true,
    numbers: false,
    symbols: false,
  });

  const generatePassword = () => {
    setPassword(
      passwordGenerator(
        length,
        settings.uppercase,
        settings.lowercase,
        settings.numbers,
        settings.symbols
      )
    );
  };

  const updateSettings = (name: string, value: boolean) => {
    setSettings({ ...settings, [name]: value });
  };

  return (
    <div className="App">
      <div className="flex mt-8 justify-center text-white">
        <div className="bg-gray-900 rounded max-w-xl p-8 ">
          <div className="text-3xl flex items-center p-2 pt-0 mb-4">
            <RiLockPasswordFill className="inline mr-2" /> Random password
            generator
          </div>

          {/* Password filed */}
          <div className="bg-gray-700 p-2  rounded">
            {password ? (
              <>
                <div>
                  <p className="text-center text-2xl break-all">{password}</p>
                </div>
                <div>
                  <CopyToClipboard
                    text={password}
                    onCopy={() => toast.dark("Password copied!")}
                  >
                    <span className="text-left cursor-pointer ">copy</span>
                  </CopyToClipboard>
                </div>
              </>
            ) : (
              <div className="uppercase text-center text-xl">
                click generate
              </div>
            )}
          </div>

          {/* length */}
          <div className="pt-2">
            <p className="text-sm font-bold text-gray-300 uppercase">
              Length: {length}
            </p>
            <div className="flex-column bg-gray-700 rounded p-2 items-center justify-center">
              <input
                type="range"
                id="length"
                name="length"
                min="4"
                max="32"
                value={length}
                onChange={(e) => setLength(parseInt(e.target.value))}
                className="w-full"
              />
            </div>
          </div>

          {/* settings */}
          <div className="pt-2 ">
            <p className="uppercase text-gray-300 text-sm font-bold">
              settings
            </p>

            <div className="bg-gray-700 rounded mt-2 flex items-center">
              <p className="inline-block p-2 pl-4 text-lg">Include Uppercase</p>
              <div className="ml-auto">
                <Switch
                  value={settings.uppercase}
                  name="uppercase"
                  updateSettings={updateSettings}
                />
              </div>
            </div>

            <div className="bg-gray-700 rounded mt-2 flex items-center">
              <p className="inline-block p-2 pl-4 text-lg">Include Lowercase</p>
              <div className="ml-auto">
                <Switch
                  value={settings.lowercase}
                  name="lowercase"
                  updateSettings={updateSettings}
                />
              </div>
            </div>

            <div className="bg-gray-700 rounded mt-2 flex items-center">
              <p className="inline-block p-2 pl-4 text-lg">Include Numbers</p>
              <div className="ml-auto">
                <Switch
                  value={settings.numbers}
                  name="numbers"
                  updateSettings={updateSettings}
                />
              </div>
            </div>

            <div className="bg-gray-700 rounded mt-2 flex items-center">
              <p className="inline-block p-2 pl-4 text-lg">Include Symbols</p>
              <div className="ml-auto">
                <Switch
                  value={settings.symbols}
                  name="symbols"
                  updateSettings={updateSettings}
                />
              </div>
            </div>
          </div>

          {/* generate button */}
          <div>
            <button
              type="button"
              className="w-full bg-gradient-to-r from-yellow-400 to-pink-500 rounded p-2 mt-2"
            >
              <p
                className="text-center text-2xl uppercase font-bold"
                onClick={generatePassword}
              >
                Generate password
              </p>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
