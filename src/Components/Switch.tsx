import React from "react";

type SwitchProps = {
  label?: string;
  value: boolean;
  name: string;
  updateSettings: (name: string, value: boolean) => void;
};

export const Switch: React.FC<SwitchProps> = ({
  label,
  name,
  value,
  updateSettings,
}) => {
  const toggleSwitch = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateSettings(e.target.name, e.target.checked);
  };
  return (
    <>
      <div className="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in">
        <input
          type="checkbox"
          name={name}
          checked={value}
          onChange={toggleSwitch}
          id={name}
          className="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer"
        />
        <label
          htmlFor={name}
          className="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"
        ></label>
      </div>
      {label && (
        <label htmlFor={name} className="text-xs text-gray-700">
          {label}
        </label>
      )}
    </>
  );
};
