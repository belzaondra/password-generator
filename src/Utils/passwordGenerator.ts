const getAsciiChars = (start: number, end: number) => {
  const chars: string[] = [];
  for (let i = start; i <= end; i++) {
    chars.push(String.fromCharCode(i));
  }
  return chars;
};

const getUppercase = () => {
  const start = 65;
  const end = 90;
  return getAsciiChars(start, end);
};

const getLowercase = () => {
  const start = 97;
  const end = 122;
  return getAsciiChars(start, end);
};

const getNumbers = () => {
  const start = 48;
  const end = 57;
  return getAsciiChars(start, end);
};

const getSymbols = () => {
  const startSymbols1 = 33;
  const endSymbols1 = 47;

  const startSymbols2 = 58;
  const endSymbols2 = 64;

  const startSymbols3 = 91;
  const endSymbols3 = 96;

  const startSymbols4 = 123;
  const endSymbols4 = 126;
  return [
    ...getAsciiChars(startSymbols1, endSymbols1),
    ...getAsciiChars(startSymbols2, endSymbols2),
    ...getAsciiChars(startSymbols3, endSymbols3),
    ...getAsciiChars(startSymbols4, endSymbols4),
  ];
};

const generateRandomNumber = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

export const generatePassword = (
  length: number,
  uppercase: boolean,
  lowercase: boolean,
  numbers: boolean,
  symbols: boolean
) => {
  const uppercaseChars = uppercase ? getUppercase() : [];
  const lowercaseChars = lowercase ? getLowercase() : [];
  const numbersChars = numbers ? getNumbers() : [];
  const symbolsChars = symbols ? getSymbols() : [];

  const possibleSymbols = [
    ...uppercaseChars,
    ...lowercaseChars,
    ...numbersChars,
    ...symbolsChars,
  ];
  if (!possibleSymbols.length) return "No symbols to choose from";

  let password = "";
  for (let i = 0; i < length; i++) {
    password +=
      possibleSymbols[generateRandomNumber(0, possibleSymbols.length - 1)];
  }

  return password;
};
